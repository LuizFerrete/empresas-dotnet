﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioIoasys.Models
{
    public class Usuario
    {
        #region Atributos
        [Key]
        public long Id { get; set; }

        [Column(TypeName = "varchar")]
        public string Email { get; set; }

        [Column(TypeName = "varchar")]
        public string Password { get; set; }

        [Column(TypeName = "varchar")]
        public string Name { get; set; }

        public bool IsAdmin { get; set; }

        public bool Ativo { get; set; }
        #endregion

        #region Atributos virtuais

        [InverseProperty("Usuario")]
        public virtual ICollection<VotoUsuario> VotosUsuario { get; set; }
        #endregion
    }
}
