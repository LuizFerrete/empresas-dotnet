﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioIoasys.Models
{
    public class VotoUsuario
    {
        #region Atributos
        [Key]
        public long IdUsuario { get; set; }

        [Key]
        public long IdFilme { get; set; }

        public int Voto { get; set; }
        #endregion

        #region Atributos virtuais
        [ForeignKey("IdUsuario")]
        [InverseProperty("VotosUsuario")]
        public virtual Usuario Usuario { get; set; }

        [ForeignKey("IdFilme")]
        [InverseProperty("VotosUsuario")]
        public virtual Filme Filme { get; set; }
        #endregion
    }
}
