﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DesafioIoasys.Models
{
    public class Filme
    {
        #region Atributos
        [Key]
        public long Id { get; set; }

        [Column(TypeName = "varchar")]
        public string Nome { get; set; }

        [Column(TypeName = "varchar")]
        public string Diretor { get; set; }

        [Column(TypeName = "varchar")]
        public string Genero { get; set; }

        [Column(TypeName = "varchar")]
        public string Ator { get; set; }

        public long IdUsuario { get; set; }

        public double MediaVotos { get; set; }
        #endregion

        #region Atributos virtuais
        [InverseProperty("Filme")]
        public virtual ICollection<VotoUsuario> VotosUsuario { get; set; }
        #endregion

    }
}
