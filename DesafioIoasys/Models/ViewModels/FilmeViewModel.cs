﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioIoasys.Models.ViewModels
{
    public class FilmeViewModel
    {
        public string Nome { get; set; }

        public string Diretor { get; set; }

        public string Genero { get; set; }

        public string Ator { get; set; }
    }
}
