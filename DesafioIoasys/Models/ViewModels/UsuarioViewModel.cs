﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioIoasys.Models.ViewModels
{
    public class UsuarioViewModel
    {
        public string Email { get; set; }

        public string Password { get; set; }

        public string Name { get; set; }

        public bool IsAdmin { get; set; }
    }
}
