﻿using DesafioIoasys.Data;
using DesafioIoasys.Models;
using DesafioIoasys.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DesafioIoasys.Classes
{
    public class FilmeDataManager : IDisposable
    {
        #region Atributos privados
        private readonly ApplicationDbContext _context;
        private readonly ClaimsPrincipal _currentUser;
        private readonly Usuario _user;
        #endregion

        #region Construtor
        public FilmeDataManager(ApplicationDbContext context,
                                ClaimsPrincipal currentUser,
                                Usuario user)
        {
            _context = context;
            _currentUser = currentUser;
            _user = user;
        }
        #endregion Construtor

        #region Métodos Privados
        private async Task<Tuple<Filme, string>> InsertFilme(FilmeViewModel filme)
        {
            string erro = ValidarFilme(filme);
            try
            {
                if (!_user.IsAdmin)
                    erro = "Usuário não é administrador.";

                if (erro != string.Empty)
                {
                    return Tuple.Create(new Filme(), erro);
                }

                Filme novoFilme = new Filme
                {
                    Nome = filme.Nome,
                    Ator = filme.Ator,
                    Diretor = filme.Diretor,
                    Genero = filme.Genero,
                    IdUsuario = _user.Id,
                    MediaVotos = 0,
                };
                await _context.Filmes.AddAsync(novoFilme);
                await _context.SaveChangesAsync();
                return Tuple.Create(novoFilme, erro);
            }
            catch (Exception e)
            {
                erro = e.Message;
                return Tuple.Create(new Filme(), erro);
            }
        }

        private string ValidarFilme(FilmeViewModel filme)
        {
            string erro = string.Empty;

            if (filme.Nome == string.Empty)
                erro = "Nome do filme obrigatório";

            if (filme.Ator == string.Empty)
                erro = "Nome do ator obrigatório";

            if (filme.Genero == string.Empty)
                erro = "Nome do gênero obrigatório";

            if (filme.Diretor == string.Empty)
                erro = "Nome do diretor obrigatório";

            return erro;
        }

        private async Task<Tuple<int, string>> InsereNota(VotoViewModel voto, long idFilme)
        {
            string erro = await ValidarVoto(voto, idFilme);
            try
            {
                if(erro != string.Empty)
                {
                    return Tuple.Create(voto.Voto, erro);
                }
                else
                {
                    VotoUsuario novoVoto = new VotoUsuario
                    {
                        IdFilme = idFilme,
                        IdUsuario = _user.Id,
                        Voto = voto.Voto
                    };

                    await _context.VotosUsuario.AddAsync(novoVoto);
                    await _context.SaveChangesAsync();


                    List<VotoUsuario> listaVotosFilme = _context.VotosUsuario.Where(i => i.IdFilme == idFilme).ToList();

                    int somaVotos = 0;
                    foreach (var votoFilme in listaVotosFilme)
                    {
                        somaVotos += votoFilme.Voto;
                    }
                    double media = 0.00;
                    media = (double)somaVotos / (double)listaVotosFilme.Count();
                    Filme filme = _context.Filmes.Find(idFilme);
                    filme.MediaVotos = media;

                    _context.Filmes.Update(filme);

                    await _context.SaveChangesAsync();
                }
            }
            catch (Exception e)
            {
                erro = e.Message;
                //gera log
            }
                return Tuple.Create(voto.Voto, erro);
        }

        private async Task<string> ValidarVoto(VotoViewModel voto, long idFilme)
        {
            string erro = string.Empty;
            try
            {
                if (voto.Voto < 0 || voto.Voto > 4)
                    erro = "O voto deve ser de 0 a 4.";

                var usuarioJaVotou = await _context.VotosUsuario.Where(v => v.IdFilme == idFilme && v.IdUsuario == _user.Id).AnyAsync();

                if (usuarioJaVotou)
                    erro = "O usuário já votou neste filme.";
            }
            catch(Exception e)
            {
                erro = e.Message;
            }
                return erro;
        }

        private async Task<Filme> GetDetalheFilme(long id)
        {
            try
            {
                Filme filme = await _context.Filmes.FindAsync(id);
                return filme;
            }
            catch (Exception e)
            {
                //gera log
                return null;
            }
        }

        private List<Filme> GetListaFilmes()
        {
            return _context.Filmes
                .OrderBy(u => u.Nome).ToList();
        }
        #endregion Métodos Privados

        #region Métodos Públicos
        public async Task<Tuple<Filme, string>> CriarFilme(FilmeViewModel filme)
        {
            return await InsertFilme(filme);
        }

        public async Task<Tuple<int, string>> VotarFilme(VotoViewModel voto, long id)
        {
            return await InsereNota(voto, id);
        }

        public async Task<Filme> DetalhesFilme(long id)
        {
            return await GetDetalheFilme(id);
        }
        public List<Filme> ListaFilmes()
        {
            return GetListaFilmes();
        }
        #endregion Métodos Públicos


        #region IDisposable Support
        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // Tarefa pendente: descartar o estado gerenciado (objetos gerenciados)
                }

                // Tarefa pendente: liberar recursos não gerenciados (objetos não gerenciados) e substituir o finalizador
                // Tarefa pendente: definir campos grandes como nulos
                disposedValue = true;
            }
        }

        // // Tarefa pendente: substituir o finalizador somente se 'Dispose(bool disposing)' tiver o código para liberar recursos não gerenciados
        // ~FilmeDataManager()
        // {
        //     // Não altere este código. Coloque o código de limpeza no método 'Dispose(bool disposing)'
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Não altere este código. Coloque o código de limpeza no método 'Dispose(bool disposing)'
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion IDisposable Support
    }
}
