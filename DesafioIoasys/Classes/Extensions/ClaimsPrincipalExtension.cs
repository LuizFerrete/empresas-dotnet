﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DesafioIoasys.Classes
{
    public static class ClaimsPrincipalExtension
    {
        public static long GetIdUsuarioLogado(this ClaimsPrincipal user)
        {
            string idUsuario = user.Claims.FirstOrDefault(c => c.Type.Equals("CdUsuario")).Value;

            return Convert.ToInt64(idUsuario);
        }

        public static bool IsAdmin(this ClaimsPrincipal user)
        {
            string isAdmin = user.Claims.FirstOrDefault(c => c.Type.Equals("IsAdmin")).Value;

            return isAdmin == "True" ? true : false;
        }
    }
}
