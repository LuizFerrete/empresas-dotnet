﻿using DesafioIoasys.Data;
using DesafioIoasys.Models;
using DesafioIoasys.Models.ViewModels;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DesafioIoasys.Classes
{
    public class UsuarioDataManager : IDisposable
    {
        #region Atributos privados
        private readonly ApplicationDbContext _context;
        private readonly ClaimsPrincipal _currentUser;
        private readonly Usuario _user;
        #endregion

        #region Construtor
        public UsuarioDataManager(ApplicationDbContext context,
                                ClaimsPrincipal currentUser, 
                                Usuario user)
        {
            _context = context;
            _currentUser = currentUser;
            _user = user;
        }
        #endregion Construtor

        #region Métodos Privados
        private string ValidaUsuario(UsuarioViewModel usuario)
        {
            string erro = string.Empty;

            if (!Regex.IsMatch(usuario.Email,
              @"^(?("")("".+?""@)|(([0-9a-zA-Z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-zA-Z])@))" +
              @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,6}))$"))
                erro = "O e-mail deve estar em um formato válido";

            if (string.IsNullOrEmpty(usuario.Name))
                erro = "O nome é obrigatório";

            return erro;
        }

        private List<Usuario> GetListaUsuarios()
        {
            return _context.Usuarios
                .Where(u => u.IsAdmin == false)
                .OrderBy(u => u.Name).ToList();
        }

        private async Task<Tuple<Usuario, string>> NovoUsuario(UsuarioViewModel usuario)
        {
            string erro = ValidaUsuario(usuario);
            try
            {
                if (!_user.Ativo)
                    erro = "Usuário inativo.";
                if (await _context.Usuarios.AnyAsync(u => u.Email == usuario.Email))
                    erro = "Usuário já existe";

                if (erro != string.Empty)
                {
                    return Tuple.Create(new Usuario(), erro);
                }

                Usuario novoUsuario = new Usuario
                {
                    Email = usuario.Email,
                    Password = SecurePassword.Hash(usuario.Password),
                    Name = usuario.Name,
                    IsAdmin = _user.IsAdmin ? usuario.IsAdmin : false, //impede usuário não admin de criar um usuário admin
                    Ativo = true,
                };
                await _context.Usuarios.AddAsync(novoUsuario);
                await _context.SaveChangesAsync();
                novoUsuario.Password = usuario.Password;
                return Tuple.Create(novoUsuario, erro); ;
            }
            catch(Exception e)
            {
                erro = e.Message;
                return Tuple.Create(new Usuario(), erro);
            }
            
        }

        private async Task<Tuple<Usuario, string>> UpdateUsuario(UsuarioViewModel usuario)
        {
            string erro = ValidaUsuario(usuario);
            try
            {
                if (!_user.Ativo)
                    erro = "Usuário inativo.";

                if (erro != string.Empty)
                {
                    return Tuple.Create(new Usuario(), erro);
                }

                Usuario usuarioOld = _context.Usuarios.Where(i => i.Email == usuario.Email).SingleOrDefault();
                if (usuarioOld == null)
                    erro = "Usuário não encontrado";

                if (usuarioOld != null)
                {
                    usuarioOld.Name = usuario.Name;
                    usuarioOld.Password = usuario.Password;
                    usuarioOld.Email = usuario.Email;
                    usuarioOld.IsAdmin = usuario.IsAdmin;

                    _context.Usuarios.Update(usuarioOld);
                    await _context.SaveChangesAsync();
                }
                return Tuple.Create(usuarioOld, erro);


            }
            catch(Exception e)
            {
                erro = e.Message;
                return Tuple.Create(new Usuario(), erro);
            }
        }

        private async Task<bool> DesativaUsuario(long id)
        {
            //verificar como reativar - verificar validações de usuário inativo
            Usuario usuario = await _context.Usuarios.FindAsync(id);

            if (_user.IsAdmin)
                usuario.Ativo = false;
            else if (!usuario.IsAdmin)
                usuario.Ativo = false;
                

            _context.Usuarios.Update(usuario);
            var result = await _context.SaveChangesAsync();

            return result > 0 ? true : false;
        }
        #endregion Métodos Privados

        #region Métodos Públicos
        public List<Usuario> ListaUsuarios()
        {
            return GetListaUsuarios();
        }

        public async Task<Tuple<Usuario, string>> CriarUsuario(UsuarioViewModel usuario)
        {
            return await NovoUsuario(usuario);
        }

        public async Task<Tuple<Usuario, string>> AtualizaUsuario(UsuarioViewModel usuario)
        {
            return await UpdateUsuario(usuario);
        }

        public async Task<bool> InativaUsuario(long id)
        {
            return await DesativaUsuario(id);
        }
        #endregion Métodos Públicos


        #region IDisposable Support
        private bool disposedValue;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // Tarefa pendente: descartar o estado gerenciado (objetos gerenciados)
                }

                // Tarefa pendente: liberar recursos não gerenciados (objetos não gerenciados) e substituir o finalizador
                // Tarefa pendente: definir campos grandes como nulos
                disposedValue = true;
            }
        }

        // // Tarefa pendente: substituir o finalizador somente se 'Dispose(bool disposing)' tiver o código para liberar recursos não gerenciados
        // ~UsuarioDataManager()
        // {
        //     // Não altere este código. Coloque o código de limpeza no método 'Dispose(bool disposing)'
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Não altere este código. Coloque o código de limpeza no método 'Dispose(bool disposing)'
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
        #endregion IDisposable Support
    }
}
