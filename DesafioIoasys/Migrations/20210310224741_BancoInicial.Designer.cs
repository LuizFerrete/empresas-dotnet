﻿// <auto-generated />
using DesafioIoasys.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace DesafioIoasys.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20210309224741_BancoInicial")]
    partial class BancoInicial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn)
                .HasAnnotation("ProductVersion", "2.1.14-servicing-32113")
                .HasAnnotation("Relational:MaxIdentifierLength", 63);

            modelBuilder.Entity("DesafioIoasys.Models.Filme", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Ator")
                        .HasColumnType("varchar");

                    b.Property<string>("Diretor")
                        .HasColumnType("varchar");

                    b.Property<string>("Genero")
                        .HasColumnType("varchar");

                    b.Property<long>("IdUsuario");

                    b.Property<decimal>("MediaVotos");

                    b.Property<string>("Nome")
                        .HasColumnType("varchar");

                    b.HasKey("Id");

                    b.HasIndex("IdUsuario");

                    b.ToTable("Filmes");
                });

            modelBuilder.Entity("DesafioIoasys.Models.Usuario", b =>
                {
                    b.Property<long>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<bool>("Ativo");

                    b.Property<string>("Email")
                        .HasColumnType("varchar");

                    b.Property<bool>("IsAdmin");

                    b.Property<string>("Name")
                        .HasColumnType("varchar");

                    b.Property<string>("Password")
                        .HasColumnType("varchar");

                    b.HasKey("Id");

                    b.HasIndex("Email")
                        .IsUnique()
                        .HasName("EmailIndex");

                    b.ToTable("Usuarios");
                });

            modelBuilder.Entity("DesafioIoasys.Models.VotoUsuario", b =>
                {
                    b.Property<long>("IdUsuario");

                    b.Property<long>("IdFilme");

                    b.Property<int>("Voto");

                    b.HasKey("IdUsuario", "IdFilme")
                        .HasName("PK_VotosUsuario");

                    b.HasAlternateKey("IdFilme", "IdUsuario");

                    b.ToTable("VotosUsuario");
                });

            modelBuilder.Entity("DesafioIoasys.Models.Filme", b =>
                {
                    b.HasOne("DesafioIoasys.Models.Usuario", "Usuario")
                        .WithMany("Filmes")
                        .HasForeignKey("IdUsuario")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("DesafioIoasys.Models.VotoUsuario", b =>
                {
                    b.HasOne("DesafioIoasys.Models.Filme", "Filme")
                        .WithMany("VotosUsuario")
                        .HasForeignKey("IdFilme")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("DesafioIoasys.Models.Usuario", "Usuario")
                        .WithMany("VotosUsuario")
                        .HasForeignKey("IdUsuario")
                        .OnDelete(DeleteBehavior.Cascade);
                });
#pragma warning restore 612, 618
        }
    }
}
