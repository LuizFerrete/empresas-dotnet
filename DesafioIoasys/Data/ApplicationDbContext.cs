﻿using DesafioIoasys.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioIoasys.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
           : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Usuario>(entity => 
            {
                entity.HasIndex(e => e.Email)
                        .HasName("EmailIndex")
                        .IsUnique();
            });

            builder.Entity<Filme>();

            builder.Entity<VotoUsuario>(entity =>
            {
                entity.HasKey(e => new { e.IdUsuario, e.IdFilme })
                    .HasName("PK_VotosUsuario");
            });
        }

        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Filme> Filmes { get; set; }
        public DbSet<VotoUsuario> VotosUsuario { get; set; }
    }
}
