Bom dia/tarde/noite! 😁

Para fazer o primeiro login, utilizar:
Email: padrao@ioasys.com
Senha: Abc123

Todas as funcionalidades foram desenvolvidas, com exceção dos items extras.

Utilizei o framework OData para fazer filtros, paginação e ordenação.
Fiz os models da maneira mais simples possível, sem criar uma tabela para Atores, Genero e Diretores, apenas Usuário e Filme.

Dava pra melhorar bastante coisa, deixar bem mais organizado tb, porém devido ao pouco tempo dado, acabou não dando tempo.

Enfim, agradeço a oportunidade! Fico no aguardo e estou a disposição 😆