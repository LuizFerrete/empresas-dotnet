﻿using DesafioIoasys.Classes;
using DesafioIoasys.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioIoasys.Services
{
    public class LoginUsuario : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool disposedValue;

        public LoginUsuario(ApplicationDbContext context)
        {
            _context = context;
        }
        public bool Login(string email, string senha)
        {
            //encrypt
            /*byte[] data = System.Text.Encoding.ASCII.GetBytes(senha);
            data = new System.Security.Cryptography.SHA256Managed().ComputeHash(data);
            String hash = System.Text.Encoding.ASCII.GetString(data);*/

            return _context.Usuarios.Any(user => user.Email.Equals(email, StringComparison.OrdinalIgnoreCase) && SecurePassword.Verify(senha, user.Password));
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // Tarefa pendente: descartar o estado gerenciado (objetos gerenciados)
                }

                // Tarefa pendente: liberar recursos não gerenciados (objetos não gerenciados) e substituir o finalizador
                // Tarefa pendente: definir campos grandes como nulos
                disposedValue = true;
            }
        }

        // // Tarefa pendente: substituir o finalizador somente se 'Dispose(bool disposing)' tiver o código para liberar recursos não gerenciados
        // ~LoginUsuario()
        // {
        //     // Não altere este código. Coloque o código de limpeza no método 'Dispose(bool disposing)'
        //     Dispose(disposing: false);
        // }

        public void Dispose()
        {
            // Não altere este código. Coloque o código de limpeza no método 'Dispose(bool disposing)'
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
