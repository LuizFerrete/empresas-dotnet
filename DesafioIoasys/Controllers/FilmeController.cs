﻿using DesafioIoasys.Classes;
using DesafioIoasys.Data;
using DesafioIoasys.Models;
using DesafioIoasys.Models.ViewModels;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DesafioIoasys.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize("Bearer")]
    public class FilmeController : ControllerBase
    {
        #region Atributos privados
        private readonly ApplicationDbContext _context;
        private readonly ClaimsPrincipal _currentUser;
        private readonly Usuario _user;
        #endregion

        #region Controller
        public FilmeController(ApplicationDbContext context,
                                  IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _currentUser = httpContextAccessor.HttpContext.User;
            _user = _context.Usuarios
                    .Where(a => a.Id == _currentUser.GetIdUsuarioLogado())
                    .SingleOrDefault();
        }
        #endregion

        #region Métodos públicos
        [HttpPost]
        public async Task<IActionResult> InsereFilme([FromBody] FilmeViewModel filme)
        {
            try
            {
                using (var filmeDataManager = new FilmeDataManager(_context, _currentUser, _user))
                {
                    var retorno = await filmeDataManager.CriarFilme(filme);
                    var filmeCriado = retorno.Item1;
                    var erro = retorno.Item2;

                    if (retorno.Item2 == string.Empty)
                        return Ok(filmeCriado);
                    else
                        return Ok(new { erro });
                }
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPost("voto/{id}")]
        public async Task<IActionResult> VotarFilme([FromBody] VotoViewModel voto, long id)
        {
            try
            {
                using (var filmeDataManager = new FilmeDataManager(_context, _currentUser, _user))
                {
                    var retorno = await filmeDataManager.VotarFilme(voto, id);
                    int notaVoto = retorno.Item1;
                    string erro = retorno.Item2;

                    if (retorno.Item2 == string.Empty)
                        return Ok(retorno.Item1);
                    else
                        return Ok(new { erro });
                }
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpGet("detalhe/{id}")]
        public async Task<IActionResult> DetalhesFilme(long id)
        {
            try
            {
                using (var filmeDataManager = new FilmeDataManager(_context, _currentUser, _user))
                {
                    var filme = await filmeDataManager.DetalhesFilme(id);
                    return Ok(filme);
                }
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        /// <summary>
        /// Listagem de filmes com filtro, paginação e ordenação via OData
        /// </summary>
        /// <remarks>
        ///     Exemplos de uso
        /// 
        ///     Para filtrar, utilizar: "/api/filme?$filter=contains(Nome, '2') eq true"
        ///     
        ///     Para paginação, utilizar: "/api/filme?$skip=10&amp;$top=10"
        ///     
        ///     Para ordenação, utilizar: "/api/filme?$orderby=mediaVotos asc"
        /// </remarks>
        /// <returns></returns>
        [EnableQuery]
        [HttpGet]
        public IActionResult ListaFilmes()
        {
            try
            {
                using (var filmeDataManager = new FilmeDataManager(_context, _currentUser, _user))
                {
                    var filmes = filmeDataManager.ListaFilmes();
                    return Ok(filmes);
                }
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        #endregion
    }
}
