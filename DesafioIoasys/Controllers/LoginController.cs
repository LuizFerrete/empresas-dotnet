﻿using DesafioIoasys.Classes;
using DesafioIoasys.Data;
using DesafioIoasys.Models;
using DesafioIoasys.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace DesafioIoasys.Controllers
{
    [AllowAnonymous]
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class LoginController : ControllerBase
    {
        #region Atributos privados
        private readonly SigningConfigurations _signingConfigurations;
        private readonly TokenConfigurations _tokenConfigurations;
        private readonly ApplicationDbContext _context;
        #endregion Atributos privados

        #region Construtor
        public LoginController([FromServices] SigningConfigurations signingConfigurations,
            [FromServices] TokenConfigurations tokenConfigurations,
            ApplicationDbContext context)
        {
            _signingConfigurations = signingConfigurations;
            _tokenConfigurations = tokenConfigurations;
            _context = context;
        }
        #endregion Construtor

        #region Métodos privados

        private async Task<int> VerificaExistenciaUsuarioPadrao()
        {
            return await _context.Usuarios.CountAsync();
        }


        private async Task<object> GerarTokenUsuario(LoginUser usuario)
        {
            var credenciaisValidas = false;
            var mensagem = "Usuário ou senha inválida";

            //gambi pra criar usuário padrão
            var countUsers = await VerificaExistenciaUsuarioPadrao();
            if (countUsers == 0)
            {
                Usuario usuarioPadrao = new Usuario
                {
                    Email = "padrao@ioasys.com",
                    Password = SecurePassword.Hash("Abc123"),
                    Name = "Usuário Padrão",
                    IsAdmin = true,
                    Ativo = true,
                };
                await _context.Usuarios.AddAsync(usuarioPadrao);
                await _context.SaveChangesAsync();
            }

            if (usuario != null && !string.IsNullOrEmpty(usuario.Email) && !string.IsNullOrEmpty(usuario.Password))
            {
                using(LoginUsuario login = new LoginUsuario(_context))
                {
                    credenciaisValidas = login.Login(usuario.Email, usuario.Password);
                }
            }

            if (credenciaisValidas)
            {
                mensagem = "Falha ao autenticar";
                ClaimsIdentity identity = new ClaimsIdentity();
                var userLogado = await _context.Usuarios.SingleOrDefaultAsync(u => u.Email == usuario.Email);

                try
                {
                    identity = new ClaimsIdentity(
                        new GenericIdentity(usuario.Email, "Login"),
                        new[]
                        {
                            new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                            new Claim(JwtRegisteredClaimNames.UniqueName, usuario.Email),
                            new Claim("IsAdmin", userLogado.IsAdmin.ToString()),
                            new Claim("CdUsuario", userLogado.Id.ToString())
                        }
                    );
                }
                catch (Exception)
                {
                    //implementar logs
                    credenciaisValidas = false;
                }

                if(userLogado.Ativo == false)
                {
                    credenciaisValidas = false;
                    mensagem = "Usuário inativo.";
                }

                if (credenciaisValidas)
                {
                    var dataCriacao = DateTime.Now;
                    var dataExpiracao = dataCriacao + TimeSpan.FromSeconds(_tokenConfigurations.Seconds);

                    try
                    {
                        var handler = new JwtSecurityTokenHandler();
                        var securityToken = handler.CreateToken(new Microsoft.IdentityModel.Tokens.SecurityTokenDescriptor
                        {
                            Issuer = _tokenConfigurations.Issuer,
                            Audience = _tokenConfigurations.Audience,
                            SigningCredentials = _signingConfigurations.SigningCredentials,
                            Subject = identity,
                            NotBefore = dataCriacao,
                            Expires = dataExpiracao
                        });
                        var token = handler.WriteToken(securityToken);

                        return new
                        {
                            authenticated = true,
                            created = dataCriacao.ToString("yyyy-MM-dd HH:mm:ss"),
                            expiration = dataExpiracao.ToString("yyyy-MM-dd HH:mm:ss"),
                            accessToken = token,
                            email = usuario.Email,
                            usuario = userLogado.Name
                        };
                    }
                    catch (Exception)
                    {
                        //implementar logs
                    }
                }
            }

            return new
            {
                authenticated = false,
                message = mensagem
            };
        }

        #endregion Métodos privados

        #region Métodos públicos

        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        [HttpPost]
        public async Task<object> Post([FromBody] LoginUser usuario)
        {
            if (usuario == null)
                return ValidationProblem();

            return await GerarTokenUsuario(usuario);
        }

        #endregion Métodos públicos
    }
}