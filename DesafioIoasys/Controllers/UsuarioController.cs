﻿using DesafioIoasys.Classes;
using DesafioIoasys.Data;
using DesafioIoasys.Models;
using DesafioIoasys.Models.ViewModels;
using Microsoft.AspNet.OData;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace DesafioIoasys.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize("Bearer")]
    public class UsuarioController : ControllerBase
    {
        #region Atributos privados
        private readonly ApplicationDbContext _context;
        private readonly ClaimsPrincipal _currentUser;
        private readonly Usuario _user;
        #endregion

        #region Controller
        public UsuarioController(ApplicationDbContext context,
                                  IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _currentUser = httpContextAccessor.HttpContext.User;
            _user = _context.Usuarios
                    .Where(a => a.Id == _currentUser.GetIdUsuarioLogado())
                    .SingleOrDefault();
        }
        #endregion

        #region Métodos públicos
        /// <summary>
        /// Listagem de usuários com paginação via OData
        /// </summary>
        /// <remarks>
        ///     Exemplos de uso
        /// 
        ///     Para paginação, utilizar: "/api/usuario?$skip=10&amp;$top=10"
        ///     
        /// </remarks>
        /// <returns></returns>
        [EnableQuery]
        [HttpGet]
        public IActionResult ListaUsuarios()
        {
            try
            {
                using (var usuarioDataManager = new UsuarioDataManager(_context, _currentUser, _user))
                {
                    if (_currentUser.IsAdmin() == false)
                        return Forbid();

                    List<Usuario> listaUsuarios = new List<Usuario>();
                    listaUsuarios = usuarioDataManager.ListaUsuarios();
                    return Ok(listaUsuarios);
                }
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPost]
        public async Task<IActionResult> InsereUsuario([FromBody] UsuarioViewModel usuario)
        {
            try
            {
                using (var usuarioDataManager = new UsuarioDataManager(_context, _currentUser, _user))
                {
                    var retorno = await usuarioDataManager.CriarUsuario(usuario);
                    var usuarioCriado = retorno.Item1;
                    var erro = retorno.Item2;

                    if (retorno.Item2 == string.Empty)
                        return Ok(usuarioCriado);
                    else
                        return Ok(new { erro });
                }
            } 
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpPut]
        public async Task<IActionResult> AtualizaUsuario([FromBody] UsuarioViewModel usuario)
        {
            try
            {
                using (var usuarioDataManager = new UsuarioDataManager(_context, _currentUser, _user))
                {
                    var retorno = await usuarioDataManager.AtualizaUsuario(usuario);
                    var usuarioCriado = retorno.Item1;
                    var erro = retorno.Item2;

                    if (retorno.Item2 == string.Empty)
                        return Ok(usuarioCriado);
                    else
                        return Ok(new { erro });
                }
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> InativaUsuario(long id)
        {
            try
            {
                using (var usuarioDataManager = new UsuarioDataManager(_context, _currentUser, _user))
                {
                    var result = await usuarioDataManager.InativaUsuario(id);
                    if (result)
                    {
                        return NoContent();
                    }
                    return BadRequest();
                }
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }
        #endregion
    }
}
